
cc.Class({
    extends: cc.Component,

    properties: {
        bgNode: cc.Node,
        pipeNode: cc.Node,
        birdNode: cc.Node,//小鸟节点
        clickLayer: cc.Node,
        startNode: cc.Node,//游戏开始节点
        scoreNode: cc.Node,
        addNode: cc.Node,
        actionList: {//小鸟动作对象：翅膀向上、翅膀向下、翅膀持平
            default: [],//可以在CocosCreator中设置动作的个数
            type: cc.SpriteFrame,
        },
        musicList: {//音乐对象：bgm、鼠标点击音、得分音
            default: [],//可以在CocosCreator中
            type: cc.AudioClip,
        },
    },

    moveBackground() {
        if (!this.isPlay) {//isPlay为false，游戏暂停
            return;
        }
        let bgList = this.bgNode.children;//获取bgNode的子节点
        // cc.log(bgList);
        for (let i = 0; i < bgList.length; i++) {//遍历bgNode的子节点
            bgList[i].x += -3;//子节点向左移动
            if (bgList[i].x <= -960) {//子节点的x值小于-960时，把子节点x的值赋为960
                bgList[i].x = 960;
            }
        }
    },

    movePipe() {
        if (!this.isPlay) {//isPlay为false，游戏暂停
            return;
        }
        let pipeList = this.pipeNode.children;//获取钢管pipeNode子节点
        for (let i = 0; i < pipeList.length; i++) {//遍历pipeNode的子节点
            pipeList[i].x += -3;//子节点向左移动
            if (pipeList[i].x <= -600) {//子节点的值小于-600时，把子节点x的值赋为600
                pipeList[i].x = 600;
                pipeList[i].active = true;//显示钢管
                pipeList[i].y = Math.round(Math.random() * 300 - 150);//随机获取钢管子节点y的值
            }
        }
    },

    moveBird() {
        if (!this.isPlay) {//isPlay为false，游戏暂停
            return;
        }
        if (!this.lockUp) {
            this.upPower += this.gravitySpeed;
        }
        this.birdNode.y += this.upPower;//小鸟位置的变化

        let birdAction = this.birdNode.getComponent(cc.Sprite);//获取小鸟节点的Sprite组件

        if (this.upPower == 0) {//翅膀平飞
            birdAction.spriteFrame = this.actionList[1];//获取平飞动作图片
            this.birdNode.angle = this.upPower;//控制小鸟角度、直接把upPower的值赋给angle
        } else if (this.upPower > 0) {//翅膀向上
            birdAction.spriteFrame = this.actionList[2];//获取翅膀向上动作图片
            this.birdNode.angle = this.upPower;//控制小鸟角度、直接把upPower的值赋给angle
        } else {//翅膀向下
            birdAction.spriteFrame = this.actionList[0];//获取翅膀向下动作图片
            this.birdNode.angle = this.upPower;//控制小鸟角度、直接把upPower的值赋给angle
        }

        if (this.birdNode.y >= 320 || this.birdNode.y <= -320) {
            this.gameOver();
        }
    },

    initGame() {
        //钢管隐藏
        let pipeList = this.pipeNode.children;
        for (let i = 0; i < pipeList.length; i++) {
            pipeList[i].active = false;
        }
        this.birdNode.x = 0;
        this.birdNode.y = 0;
        this.upPower = 0;
        this.score = 0;
        this.scoreNode.getComponent(cc.Label).string = 0;
        cc.audioEngine.play(this.musicList[0], true, 0.3);
    },

    gameStart() {
        this.initGame();
        this.isPlay = true;
        this.startNode.active = false;
    },

    gameOver() {
        this.isPlay = false;
        this.startNode.active = true;
        cc.audioEngine.stopAll();
    },

    addScore() {
        if (!this.isPlay) {
            return
        }
        this.score += 1;//分数加1
        this.scoreNode.getComponent(cc.Label).string = this.score;//把分数赋值给Label组件中的string属性
        this.addNode.x = this.birdNode.x + this.birdNode.width;//addNode.x的值
        this.addNode.y = this.birdNode.y + this.birdNode.height;//addNode.y的值
        this.addNode.active = true;//显示addNode
        this.addNode.runAction(//addNode节点执行一系列动作
            cc.sequence(//串行
                cc.spawn(//并行
                    cc.moveBy(0.3, 0, 50),//相对位移
                    cc.fadeOut(0.3)//淡出，修改的是opacity的值
                ),
                cc.callFunc(() => {//可以在执行一系列动作后，执行自己的函数
                    this.addNode.active = false;//隐藏addNode节点
                    this.addNode.opacity = 255;//不透明
                })
            )
        )
        cc.audioEngine.play(this.musicList[2], false, 0.5);//音乐、循环播放、音量
    },

    birdFly() {
        if (!this.isPlay) {
            return
        }
        this.upPower = 8;
        this.lockUp = true;
        cc.audioEngine.play(this.musicList[1], false, 0.5);//音乐、循环播放、音量

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.director.getCollisionManager().enabled = true;
    },

    start() {
        this.isPlay = false;
        this.gravitySpeed = -0.3; //重力
        this.upPower = 0;   //上升的力
        this.lockUp = false;

        //谁.on(谁的类型.EventType.事件类型,()=>{})
        this.clickLayer.on(cc.Node.EventType.TOUCH_START, () => {
            this.birdFly();
        })
        this.clickLayer.on(cc.Node.EventType.TOUCH_END, () => {
            this.lockUp = false;
        })
        this.clickLayer.on(cc.Node.EventType.TOUCH_CANCEL, () => {
            this.lockUp = false;
        })

    },

    update(dt) {
        this.moveBackground();
        this.movePipe();
        this.moveBird();
    },
});
