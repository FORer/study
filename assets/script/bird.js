
cc.Class({
    extends: cc.Component,

    properties: {

    },

    onCollisionEnter(other, self) {
        cc.log("碰撞", other.tag)
        if (other.tag == 0) {
            cc.find("Canvas/game").getComponent("game").gameOver();
        }
    },

    onCollisionExit(other, self) {
        cc.log("离开", other.tag)
        if (other.tag == 1) {
            cc.find("Canvas/game").getComponent("game").addScore();
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    // update (dt) {},
});
